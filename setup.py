from setuptools import setup, find_packages

setup(
    name="api_wrapper",
    version="1.0.0a1",
    author="Pieter De Praetere",
    author_email="pieter.de.praetere@helptux.be",
    packages=[
        "hx_wrapper",
        "hx_wrapper.rest",
        "hx_wrapper.template"
    ],
    url='https://gitlab.com/pieterdp/hx_api_wrapper',
    license='MIT',
    description="Quickly create RESTful API's for Flask.",
    long_description=open('README.txt').read(),
    install_requires=[
        'flask',
        'werkzeug',
        'requests'
    ]
)
