import abc


class ApiTemplate:
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def create(self, **kwargs):
        return

    @abc.abstractmethod
    def read(self, **kwargs):
        return

    @abc.abstractmethod
    def update(self, **kwargs):
        return

    @abc.abstractmethod
    def delete(self, **kwargs):
        return

    @abc.abstractmethod
    def list(self, **kwargs):
        return
