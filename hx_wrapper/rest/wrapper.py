import json
from werkzeug.exceptions import BadRequest
from hx_wrapper.rest import RestApi
from hx_wrapper.exceptions import *


class WrapperRestApi:
    ##
    # This table translates between API methods
    # and api_class methods. Useful when you want
    # to use another function to perform a .read().
    translation_table = {
        'post': 'create',
        'get': 'read',
        'put': 'update',
        'delete': 'delete',
        'list': 'list'
    }

    def __init__(self, api_class, o_request, api_obj_id=None, translate=None,
                 prehooks=(), posthooks=(), **kwargs):
        """
        This class is an REST API class that translates between request methods and
        methods of the api_class class.
        It performs the following functions:
            - Translate between request.method and self.action()
                GET => self.get(api_obj_id) | self.list() if api_obj_id is None
                DELETE => self.delete(api_obj_id)
                POST => self.post(o_request.get_data().decode())
                PUT => self.put(api_obj_id, o_request.get_data().decode())
            - Translate between self.action() and api_class.action() using translate or self.translation_table
                if translate is None.
            - Performs functions in hooks on the decoded but unparsed data from the original request.
                All functions take input_data_string as input and must return it (after they applied their actions).
                The order of the hooks can not be guaranteed.
            - Converts the request data to JSON.
            - Takes the reply from api_class (in JSON) and convert it to a response with the correct
                status code and headers.
            - On error: generate an error message, error code and error status code.
        :param api_class:
        :param o_request:
        :param api_obj_id:
        :param translate:
        :param prehooks:
        :param posthooks:
        :param kwargs:
        """

        self.api = api_class()
        self.request = o_request
        self.msg = None
        self.output_data = u''
        self.status_code = 200
        if translate is None:
            self.translate = self.translated_functions(self.translation_table)
        else:
            self.translate = self.translated_functions(translate)
        ##
        # Normally we would use .get_data() and convert the bytes to a string
        # but CsrfProtect() already parses the form, thus emptying .get_data()
        # We use this trick so all other functions remain unchanged.
        # Note that we still have to implement some kind of error reporting when
        # the automatic parsing fails.
        ##
        input_data_json = {}
        if self.request.method != 'GET' and self.request.method != 'DELETE':
            try:
                input_data_json = self.request.get_json(force=True, silent=False)
            except BadRequest as e:
                self.msg = str(e)
                self.status_code = 400
        if self.status_code != 400:
            try:
                for hook in prehooks:
                    input_data_json = hook(input_data_json)
            except Exception as e:
                self.msg = str(e)
                self.status_code = 400
            else:
                self.parse_request(input_data_json=input_data_json, api_obj_id=api_obj_id, **kwargs)
        ##
        # Set self.response
        ##
        try:
            for hook in posthooks:
                self.output_data = hook(self.output_data)
        except Exception as e:
            self.msg = str(e)
            self.status_code = 400
        self.response = self.create_response(self.output_data)

    def post(self, input_data, **kwargs):
        try:
            created_object = self.translate['post'](input_data=input_data, **kwargs)
        except Exception as e:
            self.msg = str(e)
            self.status_code = 400
            created_object = None
        if created_object is not None:
            return created_object
        else:
            return u''

    def get(self, item_id, **kwargs):
        try:
            found_object = self.translate['get'](item_id, **kwargs)
        except ItemNotFoundException as e:
            self.msg = str(e)
            self.status_code = 404
            found_object = None
        except Exception as e:
            self.msg = str(e)
            self.status_code = 400
            found_object = None
        if found_object is not None:
            return found_object
        else:
            return u''

    def list(self, **kwargs):
        try:
            found_objects = self.translate['list'](**kwargs)
        except Exception as e:
            self.msg = str(e)
            self.status_code = 400
            found_objects = None
        if found_objects is not None:
            out_results = []
            for found_object in found_objects:
                out_results.append(found_object)
            return out_results
        else:
            return u''

    def put(self, item_id, input_data, **kwargs):
        try:
            updated_object = self.translate['put'](item_id, input_data, **kwargs)
        except ItemNotFoundException as e:
            self.msg = str(e)
            self.status_code = 404
            updated_object = None
        except Exception as e:
            self.msg = str(e)
            self.status_code = 400
            updated_object = None
        if updated_object is not None:
            return updated_object
        else:
            return u''

    def delete(self, item_id, **kwargs):
        try:
            deleted_object = self.translate['delete'](item_id, **kwargs)
        except ItemNotFoundException as e:
            self.msg = str(e)
            self.status_code = 404
            deleted_object = None
        except Exception as e:
            self.msg = str(e)
            self.status_code = 400
            deleted_object = False
        if deleted_object is True:
            return u''
        else:
            return u''

    def parse_get(self, api_obj_id=None, **kwargs):
        """
        Parse a GET request
        :param api_obj_id:
        :return:
        """
        if api_obj_id is None:
            if hasattr(self.api, 'list'):
                self.output_data = self.list(**kwargs)
            else:
                self.msg = 'Missing argument api_obj_id'
                self.status_code = 400
        else:
            self.output_data = self.get(api_obj_id, **kwargs)

    def parse_delete(self, api_obj_id=None, **kwargs):
        """
        Parse a DELETE request
        :param api_obj_id:
        :return:
        """
        if api_obj_id is None:
            self.msg = 'Missing argument api_obj_id'
            self.status_code = 400
        else:
            self.output_data = self.delete(api_obj_id, **kwargs)

    def parse_put(self, api_obj_id=None, input_data_json=None, **kwargs):
        """
        Parse a PUT request
        :param api_obj_id:
        :param input_data_json:
        :return:
        """
        if api_obj_id is None:
            self.msg = 'Missing argument api_obj_id'
            self.status_code = 400
        else:
            if input_data_json is not None:
                self.output_data = self.put(api_obj_id, input_data_json, **kwargs)
            else:
                self.msg = 'Missing argument body'
                self.status_code = 400

    def parse_post(self, input_data_json, **kwargs):
        """
        Parse a POST request
        :param input_data_json:
        :param additional_opts:
        :return:
        """
        if input_data_json is not None:
            self.output_data = self.post(input_data_json, **kwargs)
        else:
            self.msg = 'Missing argument body'
            self.status_code = 400

    def parse_request(self, input_data_json=None, api_obj_id=None, **kwargs):
        """
        Parse the original request:
            - Check for missing arguments and input
            - Execute self.action() for the request.method (self.request)
        This function has many sub functions (parse_*) to allow for easier
        subclassing.
        :param input_data_json: (parsed JSON request data)
        :param api_obj_id:
        :param additional_opts:
        :return:
        """
        if self.request.method == 'GET':
            self.parse_get(api_obj_id, input_data_json=input_data_json, **kwargs)
        elif self.request.method == 'DELETE':
            self.parse_delete(api_obj_id, **kwargs)
        elif self.request.method == 'PUT':
            self.parse_put(api_obj_id, input_data_json=input_data_json, **kwargs)
        elif self.request.method == 'POST':
            self.parse_post(input_data_json, **kwargs)
        else:
            self.msg = 'The method {0} is not supported'.format(self.request.method)
            self.status_code = 405

    def create_response(self, data):
        """
        Create an API response
        :param data:
        :return:
        """
        rest_api = RestApi()
        if self.msg and self.msg != '':
            data = {
                'data': data,
                'msg': self.msg
            }
        return rest_api.response(status=self.status_code, data=data)

    def parse_json(self, unparsed_string):
        try:
            parsed_string = json.loads(unparsed_string)
        except ValueError as e:
            self.msg = u'A JSON error occurred: {0}'.format(e)
            return None
        return parsed_string

    def translated_functions(self, translation_table):
        """
        Take translation_table and self.api: return the function with that name in such a way
        that it can be called (https://stackoverflow.com/questions/3061/calling-a-function-of-a-module-from-a-string-with-the-functions-name-in-python)
        :return:
        """
        translation = {}
        for our_function, api_function in translation_table.items():
            call_function = getattr(self.api, api_function)
            translation[our_function] = call_function
        return translation
