import json
from flask import make_response


class RestApi:

    def __init__(self):
        self.__response = make_response()

    def response(self, status=None, data=None, headers=None):
        self.__response.data = json.dumps(data, default=lambda o: o.decode('utf-8') if hasattr(o, 'decode') else str(o))
        if status:
            self.__response.status_code = status
        else:
            self.__response.status_code = 200
        self.headers()
        if headers:
            for key, value in headers.items():
                self.__response.headers[key] = value
        return self.__response

    def headers(self):
        self.__response.headers['Content-Type'] = 'application/json'
